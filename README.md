
![preview](preview.jpg)

# Motorola Theme Atlas - Single Gated Asset

## Introduction
--------------------------------------------------------------------------------

This template has been created to assist agencies in creating a consistent experience for Motorola as it complies with branding and functional requirements for their marketing automation platform, Eloqua. The template has been optimized to function within the Eloqua environment. Outside of that, certain actions will not execute correctly, such as form submission and redirection to a Thank You page. This is expected behavior. Please use this as a guide and feel free to propose suggestions or contribute to the repository. Before beginning a new project, pull from this repository first as it will be updated periodically to maintain compliance.

**To download these files, please [click here](https://bitbucket.org/rabinovici/mot-latam-theme-atlas-dist/downloads/)**

## Editable Regions
--------------------------------------------------------------------------------

### Landing page

![Landing Page](https://motorola-solutions.s3.amazonaws.com/templates/atlas-lp.png)

**1 - Background image**
```css
.wrap-content {
		background-image: url(https://img04.en25.com/EloquaImages/clients/MotorolaSolutionsInc/%7B78a3e8a6-0375-4bf8-9ecd-7202af07f88e%7D_radios-bg-2.jpg);
		background-position: 0;
		background-size: cover;
		background-repeat: no-repeat
}
```
**2 - Left side content**

The background color can be adjusted here:
```css
.wrap-content .wrap-general .wrap-left {
		background-color: rgba(0, 94, 184, .9);
		padding: 22px;
		margin: 0 10px
}
```
The text can obviously be changed to display anything you'd like.

**3 - Asset thumbnail**
```html
<div class="col-md-4">
		<img src="https://img04.en25.com/EloquaImages/clients/MotorolaSolutionsInc/%7Bac2c8c06-e21b-498d-a859-4dbdd8764c81%7D_Cover-FPO.jpg" alt="">
</div>
```

**4 - Button**

Button text is changed via JS here:
```js
var $submintBtn = 'ENVIAR';
```
Button color is changed here:
```css
 .form-holder .container-fluid .submit-button-style {
		padding: 16px 60px;
		height: auto;
		border: none;
		border-radius: 50px;
		background: #005eb8;
		color: #fff;
		font-size: 16px;
		-webkit-transition: all .3s ease;
		transition: all .3s ease
}
```
### Thank You page

![Thank You Page](https://motorola-solutions.s3.amazonaws.com/templates/atlas-ty.png)

**1 - Background image**
```css
.wrap-content {
		background-image: url(https://img04.en25.com/EloquaImages/clients/MotorolaSolutionsInc/%7B78a3e8a6-0375-4bf8-9ecd-7202af07f88e%7D_radios-bg-2.jpg);
		background-position: 0;
		background-size: cover;
		background-repeat: no-repeat
}
```
**2 - Content box**

The background color can be adjusted here:
```css
.wrap-general__ty {
		background-color: rgba(0, 94, 184, .9);
		padding: 45px 40px 20px
}
```
The text can obviously be changed to display anything you'd like.

**3 - Header links**
```html
<div class="btn-header btn-secondary">
		<a href="#" target="_blank" data-uet='{"link-type":"link","link-label":"UPDATE_LABEL","page-area":"1"}'>
				Lorem
		</a>
</div>
<div class="btn-header btn-secondary">
		<a href="#" target="_blank" data-uet='{"link-type":"link","link-label":"UPDATE_LABEL","page-area":"1"}'>
				Lorem
		</a>
</div>
```
**4 - Asset thumbnail**

This needs to be changed in 2 places:
```html
<div class="col-md-8">
		<div class="paper--ty show-mobile">
				<div class="paper-image--ty">
						<a href="#" target="_blank" data-uet='{"link-category":"thank-you","link-type":"button","link-label":"UPDATE_LABEL","page-area":"2"}'>
								<i class="ms-up-sm"></i>
								<img src="https://img04.en25.com/EloquaImages/clients/MotorolaSolutionsInc/%7Bac2c8c06-e21b-498d-a859-4dbdd8764c81%7D_Cover-FPO.jpg">
						</a>
				</div>
		</div>
		<h1>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. </a>
		</h1>
		<p>
				Accusamus iusto minus placeat quaerat distinctio laudantium debitis dolores quas aperiam recusandae quasi veniam tempore ipsum, temporibus a vel corporis eum quisquam.
		</p>
</div>
<div class="col-md-4">
		<div class="paper--ty show-web">
				<div class="paper-image--ty">
						<a href="#" target="_blank" data-uet='{"link-category":"thank-you","link-type":"button","link-label":"UPDATE_LABEL","page-area":"2"}'>
								<i class="ms-up-sm"></i>
								<img src="https://img04.en25.com/EloquaImages/clients/MotorolaSolutionsInc/%7Bac2c8c06-e21b-498d-a859-4dbdd8764c81%7D_Cover-FPO.jpg">
						</a>
				</div>
		</div>
</div>
```
**5 - Download Button**
```html
<a href="#" target="_blank" data-uet='{"link-category":"thank-you","link-type":"button","link-label":"UPDATE_LABEL","page-area":"2"}'>
		<div class="btn-ty">
				Lorem ipsum dolor
		</div>
</a>
```

## Areas marked `CHANGE_THIS`
--------------------------------------------------------------------------------

You'll notice various areas marked in this way:

```html
<meta name="redirect-url" content="CHANGE_THIS">
```
These areas will be updated by Rabinovici and Associates before Eloqua implementation and should not be edited.

## Language specific variations
--------------------------------------------------------------------------------

We have provided 3 language types for both Landing and Thank You pages. It's important the correct file is used as the starting point as it impacts the final output. 

Items below are language specific:

- **Form**
	- Validation
	- Labels
	- Select field options
	- Button text
- **Footer**
	- Legal
	- Social media icon links
- **Tealium tracking code**
	
## Link tracking
--------------------------------------------------------------------------------

Every link on the page must be tracked. Motorola utilizes a parameter labeled `data-uet`. The parameter is included within every `<a>` tag and is structured as follows:
```html
<a href="https://www.facebook.com/MotorolaSolutionsLatAm" target="_blank" data-uet='{"link-type":"link","link-label":"facebook","page-area":"3"}'>
```
`link-label` should be a descriptive name for the link. For `page-area`, the number should be updated to correspond to the area of the page the link belongs to.

**Use the following numbers below:**

- Header: 1
- Body: 2
- Footer: 3

## Tealium tracking code
--------------------------------------------------------------------------------

Specific Tealium code is in place for both Landing and Thank You pages. Tracking will not fire if these are incorrect.

**Landing Page**
```html
<script>
		// <![CDATA[
		var uri = document.location.pathname;
		var form_name = document.getElementsByName("elqFormName")[0].value;
		var $metaLocale = document.getElementsByTagName("meta")["locale"].content;
		var utag_data = {
				"page_name": document.title,
				"page_uri": "/" + $metaLocale + "/webApp" + uri,
				"locale": $metaLocale,
				"page_type": "landing",
				"form_name": form_name
		}
		// ]]>
</script>
```
**Thank You Page**
```html
<script>
		// <![CDATA[
		var uri = document.location.pathname;
		var $metaLocaleTy = document.getElementsByTagName("meta")["locale"].content;
		var utag_data = {
				"page_name": document.title,
				"page_uri": "/" + $metaLocaleTy + "/webApp" + uri,
				"locale": $metaLocaleTy,
				"page_type": "thankyou",
				"form_name": ""
		}
		// ]]>
</script>
```

## Working with forms
--------------------------------------------------------------------------------

Please note that any element within the below `div` will be overwritten during Eloqua implementation:
```html
<div elqid="CHANGE_THIS" elqtype="UserForm">...</div>
```
Therefore, you cannot modify the form directly by manipulating the HTML. The form HTML inside of this `div` is for preview purposes only, nothing inside this container will render on the final page.

**Forms are controlled from within Eloqua. Fields are added and removed through there**.

You can, however, adjust the option fields for the both Industry and Country through JS. This is acheived by adjusting the corresponding lines which inject that code.
```js
var $selectIndustry = '' + '<option value="" selected="selected">Por favor seleccione</option>' + '<option value="Education">Educación</option>' + '<option value="Fire and Emergency Medical Services">Servicios médicos de emergencia y bomberos</option>' + '<option value="Healthcare">Cuidado de la salud</option>' + '<option value="Hospitality and Retail">Hospitalidad y venta al por menor</option>' + '<option value="Manufacturing">Fabricación</option>' + '<option value="Mining, Oil and Gas">Minería, petróleo y gas</option>' + '<option value="National Government Security">Seguridad del gobierno nacional</option>' + '<option value="Police">Policía</option>' + '<option value="Public Services (non-Public Safety Government)">Servicios públicos (Gobierno de seguridad no pública)</option>' + '<option value="Telecommunications, Finance, and General Management Services">Servicios de telecomunicaciones, finanzas y administración general</option>' + '<option value="Transportation and Logistics">Transporte y logística</option>' + '<option value="Utilities">Electricidad , Agua</option>';
var $selectCountry = '' + '<option value=""> Por favor seleccione </option>' + '<option value="AR">Argentina</option>' + '<option value="BO">Bolivia</option>' + '<option value="CL">Chile</option>' + '<option value="CO">Colombia</option>' + '<option value="CR">CostaRica</option>' + '<option value="EC">Ecuador</option>' + '<option value="SV">El Salvador</option>' + '<option value="GT">Guatemala</option>' + '<option value="HN">Honduras</option>' + '<option value="MX">Mexico</option>' + '<option value="NI">Nicaragua</option>' + '<option value="PA">Panama</option>' + '<option value="PY">Paraguay</option>' + '<option value="PE">Peru</option>' + '<option value="DO">Republica Dominicana</option>' + '<option value="PR">Puerto Rico</option>' + '<option value="UY">Uruguay</option>' + '<option value="VE">Venezuela</option>';
```